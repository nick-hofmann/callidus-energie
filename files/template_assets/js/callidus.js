var callidus = function ($) {
    var mapInstanceCounter = 0;
    var owlStage, owlSlides;
    var shrinkHeaderAfterYScrollOfPX = 100;
    var header;
    var localStorageAvailable;
    var ga_ID;
    var ga_anonymizeIp;
    var ga_disabledSuccessMessage;

    init = function () {
        header = document.querySelector("nav");
        owlStage = document.querySelector(".owl-stage");
        owlSlides = (owlStage && owlStage) ? owlStage.getElementsByClassName("owl-item") : null;
        localStorageAvailable = storageAvailable('localStorage');

        window.addEventListener('scroll', scrollHandler);
        /*window.addEventListener('resize', resizeHandler);*/

        scrollHandler(null);
        /*resizeHandler(null);*/

        // link all links with href #disableGoogleAnaltics to userDeniesGoogleAnalytics
        $("a[href$='#disableGoogleAnalytics']").on('click', userDeniesGoogleAnalyticsClickHandler);
    };

    storageAvailable = function (type) {
        try {
            var storage = window[type],
                x = '__storage_test__';
            storage.setItem(x, x);
            storage.removeItem(x);
            return true;
        }
        catch (e) {
            return false;
        }
    }

    scrollHandler = function (evt) {
        // minimize header in case of scrolling via bonzo lib
        var distanceY = window.pageYOffset || document.documentElement.scrollTop;

        // where classie is coming from is a little bit unclear
        if (distanceY > shrinkHeaderAfterYScrollOfPX) {
            classie.add(header, "smaller");
        } else {
            if (classie.has(header, "smaller")) {
                classie.remove(header, "smaller");
            }
        }
    };

    configureGoogleAnalytics = function (id, anonymizeIp, disabledSuccessMessage) {
        ga_ID = id;
        ga_anonymizeIp = anonymizeIp;

        if (getCookie('GA_allowed')) {
            embedGoogleAnalytics();
        }

        ga_disabledSuccessMessage = disabledSuccessMessage;
    };

    userDeniesGoogleAnalytics = function () {
        setCookie('GA_allowed', false, 365 * 10);
        setCookie('ga-disable-' + ga_ID, true, 365 * 10);

        alert(ga_disabledSuccessMessage);
    };

    userDeniesGoogleAnalyticsClickHandler = function (evt) {
        evt.preventDefault();
        evt.stopImmediatePropagation();
        userDeniesGoogleAnalytics();
    };

    userAllowsGoogleAnalytics = function (allowed) {
        setCookie('GA_allowed', allowed, 365 * 10);
        if (allowed) {
            embedGoogleAnalytics();
        }
    };

    embedGoogleAnalytics = function () {
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', ga_ID, 'auto');
        if (ga_anonymizeIp) {
            ga('set', 'anonymizeIp', true);
        }
        ga('send', 'pageview');
    };

    createGMap = function (mapID, address, mapLinkURL) {

        var mapDiv = document.getElementById(mapID);

        function parseAddress() {
            if (!address || !mapID) {
                mapDiv.remove();
                return;
            }

            if (mapLinkURL === undefined || mapLinkURL === '') {
                mapLinkURL = "https://www.google.de/maps/place/" + encodeURI(address);
            }

            var geocoder = new google.maps.Geocoder();
            var req = {
                address: address
            };

            if (localStorageAvailable && localStorage.getItem(address)) {
                try {
                    var coors = JSON.parse(localStorage.getItem(address));
                    if (coors) {
                        initMap(coors.lat, coors.lng);
                        return;
                    }
                }
                catch (e) {
                    console.log("lc address conversion failed");
                }
            }

            // gmaps geocode calls are restricted to max 11 calls per second
            mapInstanceCounter++;
            setTimeout(function () {
                geocoder.geocode(req, reqHandler);
            }, 250 * mapInstanceCounter);
        }

        function reqHandler(results, status) {
            if (status === 'OK') {
                try {
                    var firstResult = results[0];
                    var geometry = firstResult.geometry;
                    var location = geometry.location;
                    var coors = {
                        lat: location.lat(),
                        lng: location.lng()
                    };
                    if (localStorageAvailable) {
                        localStorage.setItem(address, JSON.stringify(coors));
                    }
                    initMap(coors.lat, coors.lng);
                }
                catch (e) {
                    console.log("gmaps API GeocoderRequest returns OK but data cannot be parsed:", e);
                }
            }
            else {
                console.log("gmaps API GeocoderRequest returns not OK:", status);
            }
        }

        function initMap(latitude, longitude) {
            var latLng = new google.maps.LatLng(latitude, longitude);
            var options = {
                zoom: 10,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                draggable: true,
                disableDoubleClickZoom: true,
                scrollwheel: false,
                mapTypeControl: false,
                overviewMapControl: false,
                streetViewControl: false,
                rotateControl: false,
                panControl: false,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL,
                    position: google.maps.ControlPosition.TOP_LEFT
                },
                scaleControl: true,
                scaleControlOptions: {
                    position: google.maps.ControlPosition.BOTTOM_LEFT
                },

            };
            map = new google.maps.Map(mapDiv, options);
            marker = new google.maps.Marker({
                map: map,
                position: latLng,
                clickable: true
            });

            if (mapLinkURL !== undefined && mapLinkURL != '') {
                google.maps.event.addListener(marker, 'click', markerClickHandler);
            }
        }

        function markerClickHandler(evt) {
            window.open(mapLinkURL, '_blank', 'resizable=yes,scrollbars=yes,toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes');
        }

        // because of jQuery ready event gmaps should be ready at this timing
        //google.maps.event.addDomListener(window, 'load', parseAddress);
        parseAddress();
    };

    //
    // helper section

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    $(document).ready(init);

    return {
        createGMap: createGMap,
        configureGoogleAnalytics: configureGoogleAnalytics,
        userAllowsGoogleAnalytics: userAllowsGoogleAnalytics,
        userDeniesGoogleAnalytics: userDeniesGoogleAnalytics
    }
}($);