var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var postcss = require('gulp-postcss');
var sass = require('gulp-sass')(require('sass'));
var autoprefixer = require('autoprefixer');
var cssnano  = require('cssnano')
var rimraf = require('rimraf')

var templateDir = './files/template_assets/css/'
var sassFiles = '/styles.scss'

gulp.task('compile-sass-dev', () => {
    return gulp.src(templateDir+sassFiles)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(templateDir));
});

gulp.task('compile-sass-live', () => {
    return gulp.src(templateDir+sassFiles)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(templateDir));
});

gulp.task('dev', () => {

    var plugins = [
        autoprefixer({})
    ];

    return gulp.src(templateDir+'*.css')
        .pipe(sourcemaps.init())
        .pipe(postcss(plugins))
        .pipe(gulp.dest(templateDir))
});

gulp.task('live', () => {

    var plugins = [
        autoprefixer({}),
        cssnano()
    ];

    rimraf(templateDir+'*.map', function (){});

    return gulp.src(templateDir+'*.css')
        .pipe(postcss(plugins))
        .pipe(gulp.dest(templateDir))
});

gulp.task('build-dev', gulp.series('compile-sass-dev', 'dev'));
gulp.task('build-live', gulp.series('compile-sass-live', 'live'));