<?php
// docu https://rocksolidthemes.com/de/contao/plugins/custom-content-elements/dokumentation
return array(
    'label' => array('Cookie Banner', ''),
    'types' => array('content', 'module'),
    'contentCategory' => 'texts',
    'moduleCategory' => 'miscellaneous',
    'wrapper' => array(
        'type' => 'none',
    ),
    'fields' => array(
        'message' => array(
            'label' => array('Text', ''),
            'inputType' => 'text',
        ),
        'dismiss' => array(
            'label' => array('Okay Button', ''),
            'inputType' => 'text',
        ),
        'linkLabel' => array(
            'label' => array('Link Text', ''),
            'inputType' => 'text',
        ),
        'linkURL' => array(
            'label' => array('Verlinkte Seite', ''),
            'inputType' => 'url',
        ),
    ),
);