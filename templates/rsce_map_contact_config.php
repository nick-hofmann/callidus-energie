<?php
// docu https://rocksolidthemes.com/de/contao/plugins/custom-content-elements/dokumentation
return array(
  'label' => array('Map Contact', ''),
  'types' => array('content', 'module'),
  'contentCategory' => 'texts',
  'moduleCategory' => 'miscellaneous',
  'standardFields' => array('cssID'),
  'wrapper' => array(
    'type' => 'none',
  ),
  'fields' => array(


    'mapAddress' => array(
      'label' => array('Google Maps Adresse', 'Vollständige Adresse, wie bei Google Maps im "Teilen Fenster" angegeben'),
      'inputType' => 'text',
    ),

  ),
);