<?php
// docu https://rocksolidthemes.com/de/contao/plugins/custom-content-elements/dokumentation
return array(
  'label' => array('CleverReach', ''),
  'types' => array('content', 'module'),
  'contentCategory' => 'texts',
  'moduleCategory' => 'miscellaneous',
  'standardFields' => array('cssID'),
  'wrapper' => array(
    'type' => 'none',
  ),
  'fields' => array(



  ),
);