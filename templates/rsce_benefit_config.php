<?php
// docu https://rocksolidthemes.com/de/contao/plugins/custom-content-elements/dokumentation
return array(
  'label' => array('Benefit', ''),
  'types' => array('content', 'module'),
  'contentCategory' => 'texts',
  'moduleCategory' => 'miscellaneous',
  'standardFields' => array('cssID'),
  'wrapper' => array(
    'type' => 'none',
  ),
  'fields' => array(

    'secondline' => array(
      'label' => array('kleine Überüberschrift', ''),
      'inputType' => 'text',
    ),

    'headline' => array(
      'label' => array('Headline', ''),
      'inputType' => 'text',
    ),

    'content' => array(
      'label' => array('Inhalt', ''),
      'eval' => array('rte' => 'tinyMCE'),
      'inputType' => 'textarea',
    ),

  ),
);