<?php
// docu https://rocksolidthemes.com/de/contao/plugins/custom-content-elements/dokumentation
return array(
  'label' => array('Clubbox', 'lorem ipsum dolor sit amet'),
  'types' => array('content', 'module'),
  'contentCategory' => 'texts',
  'moduleCategory' => 'miscellaneous',
  'standardFields' => array('cssID'),
  'wrapper' => array(
    'type' => 'none',
  ),
  'fields' => array(

      'caBoxBg' => array(
          'label' => array('Hintergrundfarbe', ''),
          'inputType' => 'select',
          'options' => array(
              'bg-hellblau' => 'hellblau',
              'bg-white' => 'weiß',
              'bg-aktivgrau' => 'hellgrau',
              'bg-secondary' => 'dunkelgrau',
              'bg-primary' => 'rot',
          ),
      ),

      'caBoxRadius' => array(
          'label' => array('Boxecken', ''),
          'inputType' => 'select',
          'options' => array(
              'rounded-lg' => 'abgerundet',
              'rounded-0' => 'spitz',
          ),
      ),

      'caBoxMT' => array(
          'label' => array('Außenabstand nach oben', ''),
          'inputType' => 'select',
          'options' => array(
              'mt-0' => '0',
              'mt-1' => '1',
              'mt-3' => '3',
              'mt-4' => '4',
              'mt-5' => '5',
          ),
      ),

      'caBoxMB' => array(
          'label' => array('Außenabstand nach unten', ''),
          'inputType' => 'select',
          'options' => array(
              'mb-0' => '0',
              'mb-1' => '1',
              'mb-3' => '3',
              'mb-4' => '4',
              'mb-5' => '5',
          ),
      ),

      'caBoxContent' => array(
          'label' => array('Inhaltselement der Box', ''),
          'elementLabel' => '%s. Inhaltselement',
          'inputType' => 'list',
          'minItems' => 1,
          'fields' => array(

              /* Auswahl */
              'caBoxElement' => array(
                  'label' => array('Boxelement', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'caBoxESubheadline' => 'Subheadline mit optionalem Icon',
                      'caBoxEHeadline' => 'Headline',
                      'caBoxEText' => 'Fließtext',
                      'caBoxELink' => 'Button / Link',
                      'caBoxEBild' => 'Bild',
                      'caBoxESVGIcon' => 'SVG Icon',
                      'caBoxEFAIcon' => 'Font Awesome Icon',
                      'caBoxEInfobar' => 'Infobar',
                  ),
              ),



              /* Subheadline */
              'caBoxContentSubheadlineIcon' => array(
                  'label' => array('FA Icon', 'Die Bezeichnung nach "fa-"'),
                  'inputType' => 'text',
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxESubheadline',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentSubheadlineText' => array(
                  'label' => array('Subüberschrift', ''),
                  'inputType' => 'text',
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxESubheadline',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentSubheadlineColor' => array(
                  'label' => array('Farbe der Subheadline', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'text-body' => 'dunkelgrau',
                      'text-white' => 'weiß',
                      'text-primary' => 'rot',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxESubheadline',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentSubheadlineMT' => array(
                  'label' => array('Abstand Oben', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mt-0' => '0',
                      'mt-3' => 'klein',
                      'mt-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxESubheadline',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentSubheadlineMB' => array(
                  'label' => array('Abstand Unten', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mb-0' => '0',
                      'mb-3' => 'klein',
                      'mb-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxESubheadline',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),



              /* Headline */
              'caBoxContentHeadlineText' => array(
                  'label' => array('Headline', ''),
                  'inputType' => 'text',
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEHeadline',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentHeadlineLink' => array(
                  'label' => array('optionaler Link der Headline', ''),
                  'inputType' => 'url',
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEHeadline',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentHeadlineTarget' => array(
                  'label' => array('Link öffnen ...', 'selbes Fenster oder neuer Tab'),
                  'inputType' => 'select',
                  'options' => array(
                      'parent' => 'im selben Fenster',
                      'blank' => 'im neuen Tab',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEHeadline',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentHeadlineStretched' => array(
                  'label' => array('Link überspannt komplette Box', 'überlagert jedes andere Element in der Box'),
                  'inputType' => 'select',
                  'options' => array(
                      'normal-link' => 'nein',
                      'stretched-link' => 'ja',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEHeadline',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentHeadlineSize' => array(
                  'label' => array('Größe der Headline', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'fs-44' => 'sehr groß',
                      'fs-32' => 'groß',
                      'fs-24' => 'mittel',
                      'fs-20' => 'klein',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEHeadline',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentHeadlineColor' => array(
                  'label' => array('Farbe der Headline', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'text-body' => 'dunkelgrau',
                      'text-white' => 'weiß',
                      'text-primary' => 'rot',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEHeadline',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentHeadlineMT' => array(
                  'label' => array('Abstand Oben', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mt-0' => '0',
                      'mt-3' => 'klein',
                      'mt-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEHeadline',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentHeadlineMB' => array(
                  'label' => array('Abstand Unten', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mb-0' => '0',
                      'mb-3' => 'klein',
                      'mb-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEHeadline',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),



              /* Fließtext */
              'caBoxContentFliesstext' => array(
                  'label' => array('Fließtext', ''),
                  'inputType' => 'textarea',
                  'eval' => array('rte' => 'tinyMCE'),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEText',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentFliesstextColor' => array(
                  'label' => array('Farbe des Fließtext', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'text-body' => 'dunkelgrau',
                      'text-white' => 'weiß',
                      'text-primary' => 'rot',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEText',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentFliesstextSize' => array(
                  'label' => array('Größe des Fließtext', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'fs-12' => 'klein',
                      'fs-16' => 'mittel',
                      'fs-20' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEText',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentFliesstextMT' => array(
                  'label' => array('Abstand Oben', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mt-0' => '0',
                      'mt-3' => 'klein',
                      'mt-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEText',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentFliesstextMB' => array(
                  'label' => array('Abstand Unten', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mb-0' => '0',
                      'mb-3' => 'klein',
                      'mb-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEText',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),



              /* Button / Link */
              'caBoxContentButtonLink' => array(
                  'label' => array('Link des Buttons', ''),
                  'inputType' => 'url',
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxELink',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentButtonTarget' => array(
                  'label' => array('Link öffnen ...', 'selbes Fenster oder neuer Tab'),
                  'inputType' => 'select',
                  'options' => array(
                      'parent' => 'im selben Fenster',
                      'blank' => 'im neuen Tab',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxELink',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentButtonText' => array(
                  'label' => array('Linktext', ''),
                  'inputType' => 'text',
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxELink',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentButtonIcon' => array(
                  'label' => array('FA Icon', 'Die Bezeichnung nach "fa-"'),
                  'inputType' => 'text',
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxELink',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentButtonColor' => array(
                  'label' => array('Farbe / Design des Buttons', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'text-body font-family-ss d-inline-block' => 'dunkelgrau',
                      'text-white font-family-ss d-inline-block' => 'weiß',
                      'text-primary font-family-ss d-inline-block' => 'rot',
                      'btn btn-primary' => 'roter Button',
                      'btn btn-light' => 'hellgrauer Button',
                      'btn btn-dark' => 'dunkelgrauer Button',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxELink',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentButtonMT' => array(
                  'label' => array('Abstand Oben', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mt-0' => '0',
                      'mt-3' => 'klein',
                      'mt-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxELink',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentButtonMB' => array(
                  'label' => array('Abstand Unten', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mb-0' => '0',
                      'mb-3' => 'klein',
                      'mb-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxELink',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),



              /* Bild */
              'caBoxContentBild' => array(
                  'label' => array('Bild', ''),
                  'inputType' => 'fileTree',
                  'eval' => array(
                      'fieldType' => 'radio',
                      'filesOnly' => true,
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEBild',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentBildAlign' => array(
                  'label' => array('Ausrichtung des Bildes', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'caImageFull' => 'volle Breite',
                      'caImageFullo' => 'volle Breite am oberen Rand',
                      'caImageFullu' => 'volle Breite am unteren Rand',
                      'caImageLeft' => 'links umfließend',
                      'caImageRight' => 'rechts umfließend',
                      'caImageCenter' => 'mittig',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEBild',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentBildHeight' => array(
                  'label' => array('Höhe des Bildes', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'caImageHa' => 'original proportional',
                      'caImageHb' => '180px',
                      'caImageHc' => '360px',
                      'caImageHd' => '540px',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEBild',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentBildMT' => array(
                  'label' => array('Abstand Oben', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mt-0' => '0',
                      'mt-3' => 'klein',
                      'mt-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEBild',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentBildMB' => array(
                  'label' => array('Abstand Unten', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mb-0' => '0',
                      'mb-3' => 'klein',
                      'mb-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEBild',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),



              /* SVG Icon */
              'caBoxContentSVGIconSVG' => array(
                  'label' => array('SVG Icon', ''),
                  'inputType' => 'fileTree',
                  'eval' => array(
                      'fieldType' => 'radio',
                      'filesOnly' => true,
                      'extensions' => 'svg',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxESVGIcon',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentSVGIconAlign' => array(
                  'label' => array('Ausrichtung des Bildes', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'caIconLeft' => 'links',
                      'caIconRight' => 'rechts',
                      'caIconCenter' => 'mittig',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxESVGIcon',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentSVGIconHeight' => array(
                  'label' => array('Größe des SVG Icons', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'caSVGHa' => 'original proportional',
                      'caSVGHb' => '64px',
                      'caSVGHc' => '128px',
                      'caSVGHd' => '256px',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxESVGIcon',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentSVGIconMT' => array(
                  'label' => array('Abstand Oben', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mt-0' => '0',
                      'mt-3' => 'klein',
                      'mt-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxESVGIcon',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentSVGIconMB' => array(
                  'label' => array('Abstand Unten', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mb-0' => '0',
                      'mb-3' => 'klein',
                      'mb-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxESVGIcon',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),




              /* FA Icon */
              'caBoxContentFAIconFA' => array(
                  'label' => array('FA Icon', 'Die Bezeichnung nach "fa-"'),
                  'inputType' => 'text',
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEFAIcon',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentFAIconAlign' => array(
                  'label' => array('Ausrichtung des Bildes', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'caIconLeft' => 'links',
                      'caIconRight' => 'rechts',
                      'caIconCenter' => 'mittig',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEFAIcon',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentFAIconColor' => array(
                  'label' => array('Farbe des Font Awesome Icons', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'text-body' => 'dunkelgrau',
                      'text-white' => 'weiß',
                      'text-primary' => 'rot',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEFAIcon',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentFAIconSize' => array(
                  'label' => array('Größe des FA Icons', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'fa-2x' => '2x',
                      'fa-3x' => '3x',
                      'fa-4x' => '4x',
                      'fa-5x' => '5x',
                      'fa-6x' => '6x',
                      'fa-7x' => '7x',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEFAIcon',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentFAIconMT' => array(
                  'label' => array('Abstand Oben', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mt-0' => '0',
                      'mt-3' => 'klein',
                      'mt-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEFAIcon',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentFAIconMB' => array(
                  'label' => array('Abstand Unten', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mb-0' => '0',
                      'mb-3' => 'klein',
                      'mb-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEFAIcon',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),



              /* Infobar */
              'caBoxContentInfoDate' => array(
                  'label' => array('Datum', 'Fakedatum'),
                  'inputType' => 'text',
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEInfobar',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentInfoTime' => array(
                  'label' => array('Lesezeit', 'Fakelesezeit'),
                  'inputType' => 'text',
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEInfobar',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentInfoColor' => array(
                  'label' => array('Farbe des Infobartextes', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'text-body' => 'dunkelgrau',
                      'text-white' => 'weiß',
                      'text-primary' => 'rot',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEInfobar',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentInfoMT' => array(
                  'label' => array('Abstand Oben', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mt-0' => '0',
                      'mt-3' => 'klein',
                      'mt-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEInfobar',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
              'caBoxContentInfoMB' => array(
                  'label' => array('Abstand Unten', ''),
                  'inputType' => 'select',
                  'options' => array(
                      'mb-0' => '0',
                      'mb-3' => 'klein',
                      'mb-5' => 'groß',
                  ),
                  'dependsOn' => array(
                      'field' => 'caBoxElement',  // Name des Feldes das geprüft werden soll
                      'value' => 'caBoxEInfobar',      // Der Wert der mit dem Feldwert übereinstimmen muss
                  ),
              ),
          ),
      ),
  ),
);