<?php
// docu https://rocksolidthemes.com/de/contao/plugins/custom-content-elements/dokumentation
return array(
  'label' => array('Studien', ''),
  'types' => array('content', 'module'),
  'contentCategory' => 'texts',
  'moduleCategory' => 'miscellaneous',
  'standardFields' => array('cssID'),
  'wrapper' => array(
    'type' => 'none',
  ),
  'fields' => array(

    'image' => array(
      'label' => array('Bild', ''),
      'inputType' => 'fileTree',
      'eval' => array(
        'fieldType' => 'radio',
        'filesOnly' => true,
      ),
    ),

    'kurzText' => array(
      'label' => array('Kurztext', ''),
      'inputType' => 'text',
    ),

    'linkURL' => array(
      'label' => array('Button Link', 'ein Button wird unter dem Text erstellt'),
      'inputType' => 'url',
    ),
    'linkTarget' => array(
      'label' => array('Button Link öffnen ...', 'selbes Fennster oder neuer Tab'),
      'inputType' => 'select',
      'options' => array(
        'parent' => 'im selben Fenster',
        'blank' => 'im neuen Tab',
      ),
    ),
    'linkText' => array(
      'label' => array('Linktext', ''),
      'inputType' => 'text',
    ),
  ),
);