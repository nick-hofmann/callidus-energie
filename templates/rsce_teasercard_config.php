<?php
// docu https://rocksolidthemes.com/de/contao/plugins/custom-content-elements/dokumentation
return array(
  'label' => array('Teasercard', ''),
  'types' => array('content', 'module'),
  'contentCategory' => 'texts',
  'moduleCategory' => 'miscellaneous',
  'standardFields' => array('cssID'),
  'wrapper' => array(
    'type' => 'none',
  ),
  'fields' => array(


      'icon' => array(
          'label' => array('FA Icon', 'Die Bezeichnung nach "fa-"'),
          'inputType' => 'text',
      ),
      'icontext' => array(
          'label' => array('Icontext', 'Der Text neben dem Icon'),
          'inputType' => 'text',
      ),



      'headline' => array(
          'label' => array('Headline', ''),
          'inputType' => 'text',
      ),




      'linkURL' => array(
          'label' => array('Link', 'der Link der ganzen Box'),
          'inputType' => 'url',
      ),
      'linkTarget' => array(
          'label' => array('Link öffnen ...', 'selbes Fennster oder neuer Tab'),
          'inputType' => 'select',
          'options' => array(
              'parent' => 'im selben Fenster',
              'blank' => 'im neuen Tab',
          ),
      ),
      'linkLabel' => array(
          'label' => array('Link Beschriftung', ''),
          'inputType' => 'text',
      ),








  ),
);