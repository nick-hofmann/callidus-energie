<?php
// docu https://rocksolidthemes.com/de/contao/plugins/custom-content-elements/dokumentation
return array(
  'label' => array('Partner', ''),
  'types' => array('content', 'module'),
  'contentCategory' => 'texts',
  'moduleCategory' => 'miscellaneous',
  'standardFields' => array('cssID'),
  'wrapper' => array(
    'type' => 'none',
  ),
  'fields' => array(

    'image' => array(
      'label' => array('Bild', ''),
      'inputType' => 'fileTree',
      'eval' => array(
        'fieldType' => 'radio',
        'filesOnly' => true,
      ),
    ),

    'headline' => array(
      'label' => array('Name', ''),
      'inputType' => 'text',
    ),

    'position' => array(
      'label' => array('Position', ''),
      'inputType' => 'text',
    ),



    'text' => array(
      'label' => array('Text', ''),
      'eval' => array('rte' => 'tinyMCE'),
      'inputType' => 'textarea',
    ),

    'tel' => array(
      'label' => array('Telefon', ''),
      'inputType' => 'text',
    ),

    'mail' => array(
      'label' => array('Email', ''),
      'inputType' => 'text',
    ),

    'linkURL' => array(
      'label' => array('Xing Link', ''),
      'inputType' => 'url',
    ),

    'linkINURL' => array(
      'label' => array('linkedIn Link', ''),
      'inputType' => 'url',
    ),

  ),
);