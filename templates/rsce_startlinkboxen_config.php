<?php
// docu https://rocksolidthemes.com/de/contao/plugins/custom-content-elements/dokumentation
return array(
  'label' => array('Icon Link Boxen', ''),
  'types' => array('content', 'module'),
  'contentCategory' => 'texts',
  'moduleCategory' => 'miscellaneous',
  'standardFields' => array('cssID'),
  'wrapper' => array(
    'type' => 'none',
  ),
  'fields' => array(

    'size' => array(
      'label' => array('Groesse', ''),
      'inputType' => 'select',
      'options' => array(
        'small' => 'klein',
        'big' => 'gross',
      ),
    ),

    'image' => array(
      'label' => array('Bild', ''),
      'inputType' => 'fileTree',
      'eval' => array(
        'fieldType' => 'radio',
        'filesOnly' => true,
      ),
    ),

    'icon' => array(
      'label' => array('FA Icon', 'Die Bezeichnung nach "fa-"'),
      'inputType' => 'text',
    ),

    'linkURL' => array(
      'label' => array('Button Link', 'ein Button wird unter dem Text erstellt'),
      'inputType' => 'url',
    ),
    'linkTarget' => array(
      'label' => array('Button Link öffnen ...', 'selbes Fennster oder neuer Tab'),
      'inputType' => 'select',
      'options' => array(
        'parent' => 'im selben Fenster',
        'blank' => 'im neuen Tab',
      ),
    ),
    'linkLabel' => array(
      'label' => array('Button Beschriftung', ''),
      'inputType' => 'text',
    ),
  ),
);