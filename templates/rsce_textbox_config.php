<?php
// docu https://rocksolidthemes.com/de/contao/plugins/custom-content-elements/dokumentation
return array(
  'label' => array('Textbox', ''),
  'types' => array('content', 'module'),
  'contentCategory' => 'texts',
  'moduleCategory' => 'miscellaneous',
  'standardFields' => array('cssID'),
  'wrapper' => array(
    'type' => 'none',
  ),
  'fields' => array(

    'smallHeadline' => array(
      'label' => array('Subüberschrift', ''),
      'inputType' => 'text',
    ),
    'headline' => array(
      'inputType' => 'standardField',
      'options' => array('h1', 'h2', 'h3', 'h4'),
    ),
    'CSS' => array(
      'label' => array('CSS Überschrift', ''),
      'inputType' => 'group',
    ),
    'hcss' => array(
      'label' => array('CSS Klasse Überschrift', 'optional'),
      'inputType' => 'text',
    ),
    'description' => array(
      'label' => array('Inhalt', ''),
      'inputType' => 'group',
    ),
    'text' => array(
      'label' => array('Text', ''),
      'inputType' => 'standardField',
      'eval' => array(
        'mandatory' => false,
      ),
    ),
    'textSize' => array(
      'label' => array('Textgröße', ''),
      'inputType' => 'select',
      'options' => array(
        'fs-12' => 'klein',
        'fs-16' => 'normal',
        'fs-20' => 'groß',
      ),
    ),
    'Verlinkung' => array(
      'label' => array('Verlinkung', ''),
      'inputType' => 'group',
    ),
    'linkURL' => array(
      'label' => array('Link am Ende des Textes', 'ein Link wird unter dem Text erstellt'),
      'inputType' => 'url',
    ),
    'linkTarget' => array(
      'label' => array('Button Link öffnen ...', 'selbes Fennster oder neuer Tab'),
      'inputType' => 'select',
      'options' => array(
        'parent' => 'im selben Fenster',
        'blank' => 'im neuen Tab',
      ),
    ),
    'linkText' => array(
      'label' => array('Linktext', ''),
      'inputType' => 'text',
    ),
  ),
);