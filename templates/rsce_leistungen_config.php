<?php
// docu https://rocksolidthemes.com/de/contao/plugins/custom-content-elements/dokumentation
return array(
  'label' => array('Leistungen', ''),
  'types' => array('content', 'module'),
  'contentCategory' => 'texts',
  'moduleCategory' => 'miscellaneous',
  'standardFields' => array('cssID'),
  'wrapper' => array(
    'type' => 'none',
  ),
  'fields' => array(

    'headline' => array(
      'label' => array('Überschrift', ''),
      'inputType' => 'text',
    ),

    'text' => array(
      'label' => array('Text', ''),
      'eval' => array('rte' => 'tinyMCE'),
      'inputType' => 'textarea',
    ),

    'linkURL' => array(
      'label' => array('Button Link', 'ein Button wird unter dem Text erstellt'),
      'inputType' => 'url',
    ),
    'linkTarget' => array(
      'label' => array('Button Link öffnen ...', 'selbes Fennster oder neuer Tab'),
      'inputType' => 'select',
      'options' => array(
        'parent' => 'im selben Fenster',
        'blank' => 'im neuen Tab',
      ),
    ),
    'linkText' => array(
      'label' => array('Linktext', ''),
      'inputType' => 'text',
    ),

  ),
);