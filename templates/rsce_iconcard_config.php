<?php
// docu https://rocksolidthemes.com/de/contao/plugins/custom-content-elements/dokumentation
return array(
  'label' => array('iconCard', ''),
  'types' => array('content', 'module'),
  'contentCategory' => 'texts',
  'moduleCategory' => 'miscellaneous',
  'standardFields' => array('cssID, image'),
  'wrapper' => array(
    'type' => 'none',
  ),
  'fields' => array(

    'headline' => array(
      'label' => array('Headline', ''),
      'inputType' => 'text',
    ),

    'content' => array(
      'label' => array('Inhalt', ''),
      'eval' => array('rte' => 'tinyMCE'),
      'inputType' => 'textarea',
    ),

    'image' => array(
      'label' => array('SVG Icon', ''),
      'inputType' => 'fileTree',
      'eval' => array(
        'fieldType' => 'radio',
        'filesOnly' => true,
        'extensions' => 'svg',
      ),
    ),

  ),
);