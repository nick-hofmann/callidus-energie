<?php
// docu https://rocksolidthemes.com/de/contao/plugins/custom-content-elements/dokumentation
return array(
  'label' => array('Termin', ''),
  'types' => array('content', 'module'),
  'contentCategory' => 'texts',
  'moduleCategory' => 'miscellaneous',
  'standardFields' => array('cssID'),
  'wrapper' => array(
    'type' => 'none',
  ),
  'fields' => array(

    'city' => array(
      'label' => array('Ort', 'Freitext'),
      'inputType' => 'text',
    ),

    'datetime' => array(
      'label' => array('Datum', 'Freitext'),
      'inputType' => 'text',
    ),

    'headline' => array(
      'label' => array('Name', ''),
      'inputType' => 'text',
    ),

    'content' => array(
      'label' => array('Beschreibung', ''),
      'eval' => array('rte' => 'tinyMCE'),
      'inputType' => 'textarea',
    ),

  ),
);